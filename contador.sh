#   ____  _          _ _         _____      _            
# / ___|| |__   ___| | |_   _  |_   _|   _| |_ ___  ___ 
# \___ \| '_ \ / _ \ | | | | |   | || | | | __/ _ \/ __|
#  ___) | | | |  __/ | | |_| |   | || |_| | || (_) \__ \
# |____/|_| |_|\___|_|_|\__, |   |_| \__,_|\__\___/|___/
#                      |___/                           
# Archivo creado por el dueño del canal shelly tutos en youtube.com
# YouTube: https://www.youtube.com/channel/UCtP_j1kK8Q2PxjyVACcv1ew
# Gitlab: https://gitlab.com/shellytutos

#!/bin/bash
declare -i count=1
while [ $count -le 999 ]
do
    echo "$count"
    count=$((count+1))
    sleep 1
done